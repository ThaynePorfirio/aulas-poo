//Crie um array vazio. Em seguida, use o método push() para adicionar 3 números ao array. Em seguida, use o método pop() para remover o último número do array e exibir o array resultante.

namespace exercicio_5{
    let numeros: number[] = [];

    /*// Adicionando um elemento ao final do array
numeros.push(1, 2, 3);
console.log(numeros); // [1, 2, 3]

// Removendo o último elemento do array
numeros.pop();
console.log(numeros); // [1, 2] */

let i = 0 
while (i < 1000) {
    numeros.push(i + 1);
    i++
}
console.log(numeros);

}