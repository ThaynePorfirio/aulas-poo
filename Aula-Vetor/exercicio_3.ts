//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.

namespace exercicio_3 {
    /*let livros: any[] = [
    {titulo: "Barraca do beijo", autor: "Beth Reekles"},
    {titulo: "Minha vida fora de serie", autor: "Paula Pimenta"},
    {titulo: "Todas as flores que não te enviei", autor: "Felipe Rocha"},
    {titulo: "Once upon a time", autor: "Adam Horowitz"}];
    
    let titulos = livros.map((livro) => {
        return livro.titulo
    });
    let autores = livros.map((livro) => {
        return livro.autor
    });
    console.log(titulos);
    console.log(autores);*/


    /*let resultado = livros.map(function (res) {
        res.titulo;
        return res.titulo;
    })
    console.log(resultado);*/

    /*Dado um array de objetos livros, contento os campos titulo e autor, crie um programaem 
    Typescript que utilize a função filter() para encontrar todos os livros do autor com valor "Autor 3".
    Em seguida, utilize a função map() para mostrar apenas os titulos dos livros encontrados.
    O resultado deve ser exibido no console.*/

    let livros: any[] =
    [
        {titulo: "titulo1", autor: "Autor 1"},
        {titulo: "titulo ab", autor: "Autor 3"},
        {titulo: "titulo af", autor: "Autor 3"},
        {titulo: "titulo4", autor: "Autor 3"}
    ];

    let autor3 = livros.filter((livro) => {
        return livro.autor === "Autor 3"
    });
    let titulos = autor3.map((livro) => 
    {
        return livro.titulo
    })
    console.log(titulos);
}

