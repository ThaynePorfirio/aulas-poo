/*Crie um vetor chamado "alunos" contendo três objetos, cada um representando um aluno com as seguintes 
propriedades: "aluno"(string) "idade" (number) "notas" (array de número). Preencha o vetor com informações 
ficticías.
Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas e 
imprima o resultado na tela, juntamente com o nome e a idade do aluno.*/

    interface Aluno {
        nome: string;
        idade: number;
        notas: number[];
    }
namespace exercicio_6 {

    const alunos: Aluno[] = [
        {nome: "Aluno 1", idade: 19, notas:[8,9,10]},
        {nome: "Aluno 2", idade: 42, notas:[7,9,8]},
        {nome: "Aluno 3", idade: 43, notas:[4,7,6]},
        {nome: "Aluno 4", idade: 86, notas:[6,7,8]},
        {nome: "Aluno 5", idade: 77, notas:[7,9,4]},
    ]

    /*alunos.forEach((aluno) => {
        console.log("------------------------------------------------------------------");
        console.log((aluno.notas[0] + aluno.notas[1] + aluno.notas[2]) / 3);
    })*/

    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce(
            (total, nota) => {return total + nota}
        ) / aluno.notas.length
        if (media >= 7) {
            console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está aprovado`);
        } else {
            console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está reprovado`);
        }
    })


}